# BitNinja file whitelisting Proof of Concept

## Prerequirements

You need to have bitninja installed and yara.
If you don't have xara, you can install it with

    yum install yara

## Restoring file times

As GIT removes original mtimes and atimes.
To combat this, an additional file named filetime-restore.php has been added.

Example usage: php filetime-restore.php --path=/path/to/the/cloned/repository

After this script, the files should have the correct m and atimes.

## detect.php

This is the detector script. You need to run this to analyze the files of a 
hosted user.

Example usage: ./detect.php /home/user/public_html

The script will do the following analysis:

1. Iterate all files, and whitelist based on the WordpresCore and plugins md5 signatures
2. Loop through the remaining files and exclude zip, pdf, jpeg, gif, and other files based on the linux file command
3. Go through the remaining files and analyze them with our yara rules
4. Create a directory in the ./results dir

After running the detect.php you can find the results in the ./results directory. The result
directories are structured in clusters based on the file creation times. To clean the unwanted files, just move
them into the _for_wuarantine directory, and then run the apply.php script.

## apply.php

Run this script to actually move the files and directories to the quarantine which you have selected by 
moving the symlink to the for_quarantine directory.
