#!/opt/bitninja-dojo/run/bin/bitninja-dojo -c=./php.ini
<?php

include("lib.php");

echo "Shrinking results directory by removing items from the \nclusters which were already selected for quarantining..\n\n";

echo "Watching directories: \n";
$dirs = scandir("./results");
foreach ($dirs as $dir){
    if (file_get_contents("./results/".$dir."/status.txt") == "not processed"){
	$watch_dirs[] = "./results/".$dir."/0_for_quarantine";
	echo " - ".$dir."\n";
    }
}

echo "\nWaiting for new directory symlinks..\n";
$processed_dirs = [];
for (;;){
    foreach ($watch_dirs as $dir){
	process_dir($dir, $processed_dirs);
	clean_empty_dirs(realpath($dir."/.."));
    }

    sleep(5);
}

function clean_empty_dirs($dir){
    $items = scandir($dir);
    foreach ($items as $item){
	$fullpath = $dir.'/'.$item;
	if ($item[0] == '0') continue;
	if (($item == '.') || ($item == '..')) continue;
	if (!is_link($fullpath) && is_dir($fullpath)) clean_empty_dirs($fullpath);
	if (!is_link($fullpath) && is_dir($fullpath) && dir_empty($fullpath)){
	    echo "Removing empty dir [".$fullpath."]\n";
	    rmdir($fullpath);
	}
    }
}

function dir_empty($dir){
    $items = scandir($dir);
    if ((sizeof($items) == 4) && (is_file($dir."/yara_report.txt") && is_file($dir."/stat.txt"))){
	unlink($dir.'/stat.txt');
	unlink($dir.'/yara_report.txt');
	return true;
    }
    if (sizeof($items) == 2)
	return true;
    return false;
}

function process_dir($dir, &$processed_dirs){
    echo "Checking dir [".$dir."]\n";
    $items = scandir($dir);
    foreach ($items as $item){
	$fullpath = $dir.'/'.$item;
	if ($item == '.' || $item == '..' || !is_dir(realpath($fullpath))) continue;
	if ($processed_dirs[$fullpath] == 1) continue;
	echo " Processing [".$fullpath."]\n";
	
	shrink(realpath($dir.'/..'), $fullpath);
	$processed_dirs[$fullpath] = 1;
    }
}

function shrink($result_dir, $path){
    $items = scandir($result_dir);
    foreach ($items as $item){
	$fullpath = $result_dir.'/'.$item;
	if (($item == '.') || ($item == '..') || ($item[0] == '0')) continue;
	
	if (is_dir($fullpath) && !is_link($fullpath)){
	    shrink($fullpath, $path);
	    continue;
	}
	
	if (is_link($fullpath)){
	    shrink_dir($result_dir.'/'.$item, $path);
	}
    }
}

function shrink_dir($dir, $path){
    $fullpath = $dir; //. '/'. $item;
    if (strpos($fullpath, realpath($path)) !== false){
	echo "Removing symlink [".$fullpath."]\n --> (".realpath($fullpath).")\n";
	//unlink($fullpath);
    }
}





