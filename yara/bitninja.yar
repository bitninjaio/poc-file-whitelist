

rule BitNinja_GenericUploader{
    strings:
	$files = "$_FILES"

    condition:
	$files
}

rule BitNinja_IcoInclude{
    strings:
	$comment = /\/\*\w{5}\*\//
	$include = "@include"
	
    condition:
	$comment and $include
}