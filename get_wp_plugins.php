#!/opt/bitninja-dojo/run/bin/bitninja-dojo -c=/opt/bitninja/etc/php.ini
<?php
// https://kinsta.com/knowledgebase/download-older-versions-of-wordpress-plugins/
// http://plugins.svn.wordpress.org/
// https://downloads.wordpress.org/plugin/wordpress-seo.11.4.zip

//$s = file_get_contents("http://plugins.svn.wordpress.org/");
//file_put_contents("wp-plugins.html", $s);

$s = file_get_contents("wp-plugins.html");
$lines = explode("\n", $s);

foreach ($lines as $line){
    if (strpos($line,"<li>") === false ) continue;
    
    $parts = explode('"', $line);
    $plugins[]=substr($parts[1], 0,-1);
}
//var_dump($plugins);

foreach ($plugins as $plugin){
    echo "\nLoading plugin [".$plugin."]\n";

    $cmd = "./collect-signatures.php svn:http://plugins.svn.wordpress.org/".$plugin."/ WordPress ".$plugin." 1.0.0";
    system($cmd);
}
