<?php


$options = getopt('p:', ['path:']);


if (!isset($options['path']) && !isset($options['p'])) {
    echo "Path not exists! Use -p=/some/path or --path=/some/path/\n";
    return;
}

$path = isset($options['path']) ? $options['path'] : $options['p'];

if (!$path) {
    echo "invalid path!";
    return;
}


try {
    $Directory = new RecursiveDirectoryIterator($path);
    $Iterator = new RecursiveIteratorIterator($Directory);
    $Regex = new RegexIterator($Iterator, '/^.+\.filemeta\.json/i', RecursiveRegexIterator::GET_MATCH);
} catch (\Exception $e) {
    echo $e->getMessage() . "\n";
    return;
}
$directories = [];

foreach ($Regex as $key => $value) {
    // that means its a dir meta
    if (strpos($key, '.dir.filemeta.json') !== false) {
        $dir = explode('.dir.filemeta.json', $key)[0];
        $directories[] = $dir;
        continue;
    } else {
        $file = explode('.filemeta.json', $key)[0];
    }

    echo 'Setting file times (' . $file . ")\n";
    $content = json_decode(file_get_contents($key), true);
    touch($file, $content['mtime'], $content['mtime']);
}

foreach ($directories as $key => $dir) {
    echo 'Setting dir times (' . $dir . ")\n";
    $content = json_decode(file_get_contents($dir . '.dir.filemeta.json'), true);
    touch($dir, $content['mtime'], $content['mtime']);
}
