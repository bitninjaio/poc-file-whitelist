#!/opt/bitninja-dojo/bin/php -c=../php.ini
<?php

echo "PHP deobfuscator by BitNinja\n";

system("rm -f gen*");
system("rm -f eval*");


$code = file_get_contents("test.php");
$sidekick = file_get_contents("sidekick.php");


$finished = false;
$i=0;

while (!$finished){
    $i++;
    $code_replaced = $code;
    normalize($code_replaced);

    $lines = explode("\n", $code_replaced);
    $lines_orig = $lines;
    $eval_snippets = [];
    foreach ($lines as $line_num=>$line){
	if ((strpos($line, "eval") === false) && (strpos($line, "preg_replace") === false)) continue;
	$snippet_name = $i."_".$line_num;
	$line_replaced = str_replace("eval(", "eval2('".$snippet_name."', ", $line);
	$line_replaced = str_replace("preg_replace(", "preg_replace2('".$snippet_name."', ", $line_replaced);

	$eval_snippets[$line_num]=$snippet_name;
	$lines[$line_num] = $line_replaced;
    }
    $code_replaced = implode("\n", $lines);

    $code_with_sk = "<?php\n".$code_replaced."\n\n".$sidekick;


    $tmp_file = 'gen_'.$i.'.php';
    file_put_contents($tmp_file, $code_with_sk);

    $out=[];
    echo "Executing file [".$tmp_file."]\n";
    exec('/usr/bin/php ./'.$tmp_file, $out);
    
    foreach ($eval_snippets as $line_num => $snippet_name){
	$snippet_result = file_get_contents('eval_'.$snippet_name);
	if ($snippet_result !== ""){
	    $lines[$line_num] = $snippet_result;
	} else {
	    $lines[$line_num] = $lines_orig[$line_num];
	}
    }

    $code = implode("\n", $lines);

    //echo $code."\n\n";

    if ((strpos($code, 'eval') === false) &&
	(strpos($code, 'preg_replace') === false)){
	$finished = true;
	normalize($code);
	file_put_contents("gen_x.php", "<?php\n".$code);
    }

    if ($i == 10){
	$finished = true;
    }
}

function normalize(&$code){
    $code = str_ireplace("eval", "\neval",$code);
    $code = str_ireplace(";", ";\n",$code);
    $code = str_ireplace("\n\n", "\n",$code);
    $code = str_ireplace("eval ", "eval",$code);
    $code = str_ireplace("\r", "",$code);

    $code = str_ireplace("preg_replace", "\npreg_replace",$code);
    $code = str_ireplace("preg_replace ", "preg_replace",$code);
}


