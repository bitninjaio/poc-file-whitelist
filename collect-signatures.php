#!/opt/bitninja-dojo/run/bin/bitninja-dojo -c=php.ini
<?php

include("lib.php");


if (!isset($argv[1])){
    echo "No repository specified!\n";
    echo "Usage: collect-signatures.php [repo-url.tar.gz] [CMS_name] [component] [version] --keep-files\n";
    echo " repo-url: example: https://github.com/WordPress/WordPress.git\n";
    echo "           Start the url with svn: for svn repo!";
    echo " CMS_name: The name of the CMS system example. WordPress\n";
    echo " component: The name of the subsystem, them, plugin etc.\n";
    echo " varsion: Version number";
    echo " --keep-files Don't delete files from the src directory.";
    exit;
}

$url = $argv[1];
$CMS_name = $argv[2];
$component = $argv[3];
$version = $argv[4];
$keep_files = $argv[5];

if (!isset($keep_files)) {
    download_src($url);
}

$cms_database = load_cms_database();
$cms_id = get_cms_id($CMS_name, $component, $version, $cms_database);
echo "CMS ID: ".$cms_id."\n";
$md5_database = load_md5_database_strip();


if (strpos($url, "svn:") !== false){
    // Iterate through tags as versions.
    $versions = scandir('src/tags');
    $has_tags = false;
    foreach ($versions as $version_dir){
	if (($version_dir == '.') || ($version_dir == '..')) continue;
	
	$cms_id = get_cms_id($CMS_name, $component, $version_dir, $cms_database);
	do_collect($md5_database, $cms_database, $cms_id, 'src/tags/'.$version_dir);
	$has_tags =true;
    }
    
    if (!$has_tags){
	$cms_id = get_cms_id($CMS_name, $component, 'latest', $cms_database);
	do_collect($md5_database, $cms_database, $cms_id, 'src/trunk/');
    }
    exit;
}

do_collect($md5_database, $cms_database, $cms_id, 'src');


function do_collect(&$md5_database, &$cms_database, $cms_id, $dir ){
    echo "Collecting file hashes from src directory ..\n";

    recursive_collect($dir, $md5_database, $cms_id, getcwd().'/'.$dir);
    save_md5_database_strip($md5_database);
    save_cms_database($cms_database);

    echo "\nDone.\n";
}

