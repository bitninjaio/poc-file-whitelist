#!/opt/bitninja-dojo/run/bin/bitninja-dojo -c=./php.ini
<?php

include("lib.php");

echo "Applying requested actions based on the ./results directory.\n";

$dirs = scandir('./results');

foreach ($dirs as $dir){
    $dirpath = './results'.'/'.$dir;
    if (!is_dir($dirpath)) continue;
    if (($dir == '.') || ($dir == '..')) continue;
    
    $statusfile = $dirpath.'/status.txt';
    if (!is_file($statusfile)){
	echo "Warning! Missing status file in [".$dir."]\n";
	continue;
    }
    if (file_get_contents($statusfile) !== 'not processed') continue;
    
    echo "Processing result directory [".$dir."]\n";
    
    $quar_dir = $dirpath.'/0_for_quarantine';
    $quar_files = scandir($quar_dir);

    $quarantined_dir = $dirpath.'/0_quarantined';
    if (!is_dir($quarantined_dir)) mkdir($quarantined_dir);
    
    foreach ($quar_files as $quar_symlink){
	if (($quar_symlink == '.') || ($quar_symlink == '..')) continue;
	// if (is_dir($quar_dir.'/'.$quar_symlink)) continue;
	$real_file = readlink($quar_dir.'/'.$quar_symlink);
	echo 'Quarantaining file or directory ['.$real_file."]\n";
	md5_greylist($real_file);
	$quarantined_path = quarantaine($real_file);
	$prefix = '';
	if (basename($quar_symlink)[0] == 'Y') $prefix = 'Y__';
	symlink($quarantined_path, $quarantined_dir."/".$prefix.basename($quar_symlink));
	unlink($quar_dir.'/'.$quar_symlink);
    }
    
    file_put_contents($statusfile, 'processed');
}

function quarantaine($file_path){
    $uid = fileowner($file_path);
    $user_info =posix_getpwuid($uid);
    $homedir = $user_info["dir"];
    $quar_dir = '/var/lib/bitninja/quarantine/'.date("Y/m/d").$homedir;
    mkdir($quar_dir, 0700, true);
    
    // create .info content
    $rnd_id = rand(0, 32767);

    $path_parts = pathinfo($file_path);
    $file_name = basename($file_path);
    $dir = dirname($file_path);

    $file_owner = fileowner($file_path);
    $file_group = filegroup($file_path);
    $file_perms = fileperms($file_path);
    $file_size = filesize($file_path);
    $file_created = date("Y-m-d H:i:s", filectime($file_path));

    $quar_path = $quar_dir . DIRECTORY_SEPARATOR . $rnd_id . "_" . $path_parts['basename'];

    $info_content = $file_owner . " " . $file_group . " " . intval($file_perms) . " " . $file_path . " " . $malware_name . " "
	. $file_size . " " . $file_created;
    
    file_put_contents($quar_path.'.info', $info_content);
    if (is_dir($file_path)){
	rmove($file_path, $quar_path);
    } else {
	rename($file_path, $quar_path);
    }

    return $quar_path;
}


