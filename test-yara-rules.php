#!/opt/bitninja-dojo/run/bin/bitninja-dojo -c=./php.ini
<?php

echo "Testing YARA rules on all quarantined files.\n";

$result_dirs = scandir("./results");

foreach ($result_dirs as $result_dir){
    if (!is_dir('./results/'.$result_dir)) continue;
    if (($result_dir == '.') || ($result_dir == '..')) continue;

    $quar_files = scandir('./results/'.$result_dir."/0_quarantined");
    foreach ($quar_files as $quar_file){
	$file = "./results/".$result_dir.'/0_quarantined/'.$quar_file;
	if ($quar_file[0] == 'Y') continue;
	
	if (!is_file($file)) continue;
	if (($quar_file == '.') || ($quar_file == '..')) continue;
	
	$cmd = "yara ./yara/master.yar --no-warnings ".escapeshellarg($file);
	$res = exec($cmd);
	if (strlen($res) == 0){
	    echo ".";
	    $undetected[]=$file;
	    continue;
	}
	$res_parts = explode(' ', $res);
	$hits[$file] = $res_parts[0];
	echo "+";
    }
}

echo "\nDetected files:\n";
foreach ($hits as $file =>$name){
    echo $file." => ".$name."\n";
}

echo "\n\nUndetected files:\n";

foreach ($undetected as $f){
    echo " ".$f."\n";
}

echo "\nUndetected files count: ".count($undetected)."\n";
