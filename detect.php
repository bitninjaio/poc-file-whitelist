#!/opt/bitninja-dojo/run/bin/bitninja-dojo -c=./php.ini
<?php

include("lib.php");

if (! isset($argv[1])){
    echo "Usage: detect.php [/path/po/public_html]";
    exit;
}

$dir = realpath($argv[1]);

$cms_database = load_cms_database();
$md5_database = load_md5_database_strip();

$resultset = [];
echo "Discovering whitelisted files..\n";
inspect_directory($dir, $cms_database, $md5_database, $resultset, $dir);
echo_resultset_stats($resultset);
echo "\n\n";

echo "Analysing file types..\n";
analyze_file_types($resultset);
echo_resultset_stats($resultset);
echo "\n\n";

echo "Analyzing files with YARA rules..\n";
analyze_yara($resultset);
echo "\n\n";

echo "Generating result dirtectory..\n";
save_result_dir($resultset, $dir);
echo "\n\n";


echo "\nDone. See results in ./results directory! \n";

function echo_resultset_stats(&$resultset){

    foreach ($resultset['clusters'] as $cluster){
	$cluster_cnt++;
	foreach ($cluster as $item){
	    if (is_dir($item)) $dir_cnt++; else $file_cnt++;
	}
    }

    echo "\nFound [".$cluster_cnt."] clusters, [".$dir_cnt."] dirs and [".$file_cnt."] files for further analysis.\n";

}

function save_result_dir(&$resultset, $dir){
    $user_info = posix_getpwuid(fileowner($dir));
    $dir_user = $user_info['name'];
    $res_basedir = 'results/'.date("Y-m-d_H_i_").str_replace("/", "_", $dir_user);
    
    $all_files = "";
    foreach ($resultset['clusters'] as $date=>$cluster){
	$cluster_dir = $res_basedir.'/'.$date;
	    //str_replace(' ', '_', str_replace(":", "_", $date));
	$cluster_dir.="__".count($cluster)."-".count($resultset['yara'][$date]);
	mkdir($cluster_dir, 0644, true);
	
	$yara_report = "";
	// Process dirs
	$dirs = [];
	foreach ($cluster as $item){
	    if (!is_dir($item)) {
		$dirs[]=dirname($item);
	    } else {
		$dirs[] = $item;
	    }
	}

	$dirs_orig = $dirs;
	cut_common_dir($dirs, $common);

	foreach($dirs as $id => $dir){
	    $dir_md5 = substr(md5($dir),0,3);
	    mkdir($cluster_dir.'/'.$dir, 0600, true);
	    symlink($dirs_orig[$id], $cluster_dir.'/'.$dirs[$id].'_'.$dir_md5);
	}

	foreach ($cluster as $file){
	    if (is_dir($file)) continue;
	    
	    $flags = "";
	    if (isset($resultset['yara'][$date][$file])){
		$flags = "_Y";
		$yara_report.= $file. ":".$resultset['yara'][$date][$file]."\n";
	    }
	    
	    if (isset($resultset['greylisted'][$date][$file])){
		$flags = "_Y";
		$yara_report.= $file. ":".$resultset['yara'][$date][$file]."\n";
	    }
	    
	    $dir_md5 = substr(md5(dirname($file)),0,3);
	    $target_file = $cluster_dir.'/'.substr($file, strlen($common)).$flags.'_'.$dir_md5;

	    symlink($file, $target_file);
	    echo ".";
	}
	
	file_put_contents($cluster_dir."/yara_report.txt", $yara_report);
	
	$stat ="Filecount:".count($cluster)."\n";
	$wlcount = isset($resultset['cluster-info'][$date]['whitelisted-files']) ? $resultset['cluster-info'][$date]['whitelisted-files'] : 0;
	$stat.="Whitelisted:".$wlcount."\n";
	$stat.="Yara:".count($resultset['yara'][$date])."\n";
	$stat.="Greylisted:".$resultset['cluster-info'][$date]['greylisted-files']."\n";
	
	file_put_contents($cluster_dir.'/stat.txt', $stat);
    }
    mkdir($res_basedir.'/0_for_quarantine');
    mkdir($res_basedir.'/0_for_whitelist');
    file_put_contents($res_basedir.'/status.txt', 'not processed');
    
    
    foreach ($resultset['yara_stats'] as $rule => $count){
	$yara_stat_s .= $rule.':'.$count."\n";
    }
    file_put_contents($res_basedir.'/yara_stats.txt', $yara_stat_s);
}


	// /home/user1/public_html/hack
	// /home/user1/public_html/hack/1
	// /home/user1/public_html/aaa/22
	// /home/user1/public_html/hack/b
	// public_html should stay
function cut_common_dir(&$dirs, &$common){
    $dirs_orig = $dirs;
    $chars=str_split($dirs[0]);
    $common='';
    foreach ($chars as $index =>$c){
	if ($c == '/') $pos = $index;
	foreach ($dirs as $id =>$dir){
	    if ($dir[$index] !== $c) break 2;
	}
    }
    
    foreach ($dirs as $id =>$dir){
	$dirs[$id]=substr($dir,$pos);
    }
    $common=substr($dirs_orig[0], 0, $pos);
    
    if (sizeof($dirs) == 1){
	$common = dirname($dirs_orig[0]).'/';
	$dirs[0] = basename($dirs_orig[0]);
    }
}

function analyze_file_types(&$resultset){
    foreach ($resultset['clusters'] as $date=>$cluster){
	foreach ($cluster as $id => $file){
	    if (is_dir($file)) continue;
	    //echo $file. "\n";
	    $dump = shell_exec('file -bi '.escapeshellarg($file));
	    $info = explode(';', $dump);
	    $mime_type = $info[0];
	    $mime = explode('/',$mime_type);
	    if ( ($mime[0] == 'image') || ($mime[0] == 'audio') || ($mime[1] == 'pdf') || ($mime[1] == 'zip') || ($mime[1] == 'msword')) {
		unset($resultset['clusters'][$date][$id]);
		if (sizeof($resultset['clusters'][$date]) == 0){
		    unset($resultset['clusters'][$date]);
		}
		echo "-";
		continue;
	    }
	    echo "!";
	}
    }
}

function analyze_yara(&$resultset){
    foreach ($resultset['clusters'] as $date=>$cluster){
	foreach ($cluster as $id => $file){
	    if (is_dir($file)) continue;
	    $res_lines = shell_exec('yara ./yara/master.yar '.escapeshellarg($file));
	    if (strlen($res_lines) == 0) {
		echo ".";
		continue;
	    }
	    
	    // Weevely_Webshell /home/wwwngtripplegee/public_html//admin/server/php/files/12.php
	    $yara_parts = explode(' ', $res_lines);
	    $resultset['yara'][$date][$file] = $yara_parts[0];
	    $resultset['yara_stats'][$yara_parts[0]]++;
	    echo 'Y';
	}
    }
}
