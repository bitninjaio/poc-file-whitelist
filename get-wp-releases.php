#!/opt/bitninja-dojo/run/bin/bitninja-dojo
<?php

//system("wget https://wordpress.org/download/releases/ -O wp-release.html");

$s = file_get_contents("wp-release.html");
// var_dump($s);

$parts = explode("\"",$s);

foreach ($parts as $word){
    if ($word == '') continue;
    if (strpos($word, ".zip") !== false){
	if (strpos($word, "md5") !== false) continue;
	if (strpos($word, "sha1") !== false) continue;
	if (strpos($word, "IIS") !== false) continue;
	
	$versions[]= substr(substr($word,32), 0, -4);
    }
}

echo "'".implode("','", $versions)."'";
