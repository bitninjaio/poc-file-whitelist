#!/opt/bitninja-dojo/run/bin/bitninja-dojo
<?php

function inspect_directory($dir, &$cms_database, &$md5_database, &$resultset, $basedir){
    if ((strpos(realpath($dir), $basedir) !== 0) || (strlen(realpath($dir))) < strlen(realpath($basedir))){
	echo "Dir: ".realpath($dir)."\n";
	echo "Basedir: ".$basedir."\n";
	echo "!!! Warning! External symlink detected: [".$dir."]";
	return;
    }

    $files = scandir($dir);
    foreach($files as $key => $value){
	if($value == "." || $value == ".." || $value == '.git') continue;

        $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
	$relative_path = substr($path, $cwd_len);
	inspect_item($relative_path, $cms_database, $md5_database, $resultset);
	if (is_dir($path)){
	    inspect_directory($path, $cms_database, $md5_database, $resultset, $basedir);
        }
    }
}

function inspect_item($item, &$cms_database, &$md5_database, &$resultset){
    $ctime = filemtime($item);
    $time_bucket = date("Y-m-d H", $ctime);

    if (is_dir($item)){
    	//$resultset['clusters'][$time_bucket][] = $item;
    	return;
    }
    
    $file = $item;
    if (filesize($file) > 1024*1024*5) return;
    //echo $file."\n";
    

    $md5 = md5_file($file);
    $info = $md5_database[$md5];
    if ($info === null ){
        echo "!";
        $ctime = filemtime($file);
        //$rounded_time = 
        $resultset['clusters'][$time_bucket][] = $file;
        return;
    }
    $resultset['cluster-info'][$time_bucket]['whitelisted-files']++;
    
    if (md5_greylist_contains($file, $md5)){
	echo 'G';
	$resultset['cluster-info'][$time_bucket]['greylisted-files']++;
	$resultset['greylisted'][$time_bucket][$file] = 1;
    }
    
    echo ".";
}

function recursive_collect($dir, &$md5_database, $cms_id, $basedir){
    $files = scandir($dir);
    foreach($files as $key => $value){
        $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
        if(!is_dir($path)) {
	    $relative_path = substr($path, strlen($basedir));
	    save_hash($relative_path, $basedir, $md5_database, $cms_id);
        } else if($value != "." && $value != ".." && $value !== '.git') {
            recursive_collect($path, $md5_database, $cms_id, $basedir);
        }
    }
}


function save_md5_database(&$md5_database, &$cms_database){
    file_put_contents('_md5.json', json_encode($md5_database, JSON_PRETTY_PRINT));
}

function save_md5_database_strip(&$md5_database){
    foreach ($md5_database as $md5 => $info){
	$res .= $md5."\n";
    }
    file_put_contents('_md5_strip.txt', $res);
}

function load_md5_database(){
    if (!is_file('_md5.json')){
	return [];
    }
    return json_decode(file_get_contents('_md5.json'), true);
}

function load_md5_database_strip(){
    $s = file_get_contents('_md5_strip.txt');
    $lines = explode("\n", $s);
    $md5=[];
    foreach ($lines as $line){
	$md5[trim($line)] = 1;
    }
    return $md5;
}


function save_cms_database(&$cms_database){
    file_put_contents('_cms.json', json_encode($cms_database, JSON_PRETTY_PRINT));
}

function load_cms_database(){
    if (!is_file('_cms.json')){
	return [];
    }
    return json_decode(file_get_contents('_cms.json'), true);
}

function get_cms_id($CMS_name, $component, $version, &$cms_database){
    foreach ($cms_database as $id =>$info){
	if (($info['name'] == $CMS_name) &&
	    ($info['component'] == $component) &&
	    ($info['version'] == $version )) return $id;
    }
    
    $info['name'] = $CMS_name;
    $info['component'] = $component;
    $info['version'] = $version;
    $cms_database[] = $info;
    
    return get_cms_id($CMS_name, $component, $version, $cms_database);
}

function save_hash($path, $basedir, &$md5_database, $cms_id){
    $md5 = md5_file($basedir."/".$path);
    if (isset($md5_database[$md5])){
	echo "o";
	$record = $md5_database[$md5];
	foreach ($record[1] as $r_cms_id){
	    if ($r_cms_id == $cms_id)
		return;
	}
	$record[1][] = $cms_id;
        $md5_database[$md5] = $record;
        return;
    }
    
    $record = [$path, [$cms_id]];
    $md5_database[$md5] = $record;
    echo "N";
}

function download_src($url){
    if (is_dir("src")){
        echo "Removing src directory recursively...\n";
        system("rm -rf ./src");
    }

    mkdir("src");
    if (strpos($url, '.git') !== false){
	download_src_git($url);
    }
    
    if (strpos($url, 'svn:') !== false){
	download_src_svn($url);
    }
    
    if (strpos($url, '.tar.gz') !== false){
	download_src_tar_gz($url);
    }
    
    if (strpos($url, '.zip') !== false){
	download_src_zip($url);
    }

}

function download_src_git($url){
    echo "Cloning source repository..\n";
    $cmd = "git clone ".escapeshellarg($url)." src";
    $res = exec($cmd);
}

function download_src_svn($url){
    echo "Checking out source repository..\n";
    // Cut the initial svn: part
    $url = substr($url, 4);
    $cmd = "svn co ".escapeshellarg($url)." src";
    $res = exec($cmd);
}

function download_src_tar_gz($url){
    echo "Downloading source files..\n";
    $cmd = "wget ".escapeshellarg($url)." -O src.tar.gz";
    $res = exec($cmd);
    $cmd = "tar -xvf src.tar.gz --strip 1 --directory src";
    $res = exec($cmd);
    unlink('src.tar.gz');
}


function download_src_zip($url){
    echo "Downloading source files..\n";
    $cmd = "wget ".escapeshellarg($url)." -O src.tar.gz";
    $res = exec($cmd);
    $cmd = "tar -xvf src.tar.gz --strip 1 --directory src";
    $res = exec($cmd);
    unlink('src.tar.gz');
}

/**
 * Recursively move files from one directory to another
 * 
 * @param String $src - Source of files being moved
 * @param String $dest - Destination of files being moved
 */
function rmove($src, $dest){    
    echo "    move [".$src."] -->".$dest."\n";
    // If source is not a directory stop processing
    if(!is_dir($src)) return false;

    // If the destination directory does not exist create it
    if(!is_dir($dest)) { 
        if(!mkdir($dest)) {
            // If the destination directory could not be created stop processing
            return false;
        }    
    }

    // Open the source directory to read in files
    $i = new DirectoryIterator($src);
    foreach($i as $f) {
	if ($f->isLink()){
	    echo " moving link:". $f->getPathname()."\n";
	    rename($f->getPathname(), "$dest/" . $f->getFilename());
	}
    
        if($f->isFile()) {
    	    echo " moving file:". $f->getPathname()."\n";
    	    md5_greylist($f->getRealPath());
            rename($f->getRealPath(), "$dest/" . $f->getFilename());
        }
        
        if(!$f->isDot() && $f->isDir() && !$f->isLink()) {
            rmove($f->getRealPath(), "$dest/$f");
	    if ($f->isFile()) unlink($f->getRealPath());
	    if ($f->isDir()) rmdir($f->getRealPath());
        }
    }
    rmdir($src);
}


function md5_greylist($file, $md5=''){
    global $md5_greylist_cache;
    
    if ($md5 == ''){
	$md5=md5_file($file);
    }

    // Don t greylist empty file hash
    if ($md5 == "d41d8cd98f00b204e9800998ecf8427e") return;
    // Do nothing if the hash is already greylisted.
    if (md5_greylist_contains($file, $md5)) return;

    $s=$md5."\n";
    file_put_contents("_md5_greylist.txt", $s, FILE_APPEND);
    $md5_greylist_cache=file_get_contents("_md5_greylist.txt");
    $path='./greylist_files/'.$md5[0].'/'.$md5[1];
    mkdir($path, 0600, true);
    file_put_contents($path.'/'.$md5, file_get_contents($file));
}


function md5_greylist_contains($file, $md5=''){
    global $md5_greylist_cache;
    
    if ($md5 == ''){
	$md5=md5_file($file);
    }
    
    if (!isset($md5_greylist_cache)){
        $s=file_get_contents("_md5_greylist.txt");
        $md5_greylist_cache=$s;
    } else {
	$s=$md5_greylist_cache;
    }
    
    if (strpos($s, $md5) !== false){
	return true;
    }
    return false;
}
